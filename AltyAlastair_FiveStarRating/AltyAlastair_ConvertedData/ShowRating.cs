﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;
using System.Data;
namespace AltyAlastair_ConvertedData
{
    class ShowRating
    {
        public MySqlConnection conn = null;
        public static void Rating()
        {
            string cs = @"server=172.16.1.1;userid=dbsAdmin;password=password;database=SampleRestaurantDatabase;port=8889";

            ShowRating instance = new ShowRating();

            instance.conn = new MySqlConnection(cs);
            // Connection string
            try
            {

                // try to open the database.
                instance.conn = new MySqlConnection(cs);
                instance.conn.Open();


                // Prompt
                Console.WriteLine("Hello, how would you like to sort the data? Type the number associated with your choice.");
                Console.Write(
                                        "1.) List Restaurants Alphabetically(Show Rating Next To Name\r\n" +
                                        "2.) List Restaurants in Reverse Alphabetical (Show Rating Next To Name)\r\n" +
                                        "3.) Sort Restaurants From Best to Worst (Show Rating Next To Name)\r\n" +
                                        "4.) Sort Restaurants From Worst to Best (Show Rating Next To Name)\r\n" +
                                        "5.) Show Only X and Up\r\n" +
                                        "6.) Exit\r\n" +
                                        "");
                // Catch user's choice
                string userChoice = Console.ReadLine();
                int choice;

                // Validate
                while (!int.TryParse(userChoice, out choice))
                {
                    Console.WriteLine("That was an invalid entry please only enter a number for your selection (1-6) ");
                    userChoice = Console.ReadLine();
                }
                if (choice == 1)
                {
                    //SQL statement
                    string stm = "SELECT RestaurantName, OverallRating FROM RestaurantProfiles ORDER BY RestaurantName ASC";
                    
                    // COmmand object
                    MySqlCommand cmd = new MySqlCommand(stm, instance.conn);
                    // Execute and run
                    MySqlDataReader rdr = cmd.ExecuteReader();

                    // Read info
                    while (rdr.Read())
                    {

                        string restName = rdr["RestaurantName"].ToString();
                        string rating = rdr["OverallRating"].ToString();
                        double stars;
                        if (double.TryParse(rating, out stars))
                        {
                            
                            stars = Math.Round(stars, MidpointRounding.AwayFromZero);

                            if (stars == 0)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("*****");
                            }

                            if (stars < 1.50 && stars > 0)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("****");
                            }
                            if(stars > 1.49 && stars < 2.50)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("***");
                            }
                            if (stars > 2.49 && stars < 3.50)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("**");

                            }
                            if (stars > 3.49 && stars < 4.50)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("*");


                            }

                            if (stars > 4.49)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.WriteLine($"{starCount}");


                            }
                            Console.ForegroundColor = ConsoleColor.White;

                        }

                    }
                    Console.WriteLine("Press any key to return to the main menu.");
                    Console.ReadKey();
                    Console.Clear(); ;
                }
                else if (choice == 2)
                {
                    //SQL statement
                    string stm = "SELECT RestaurantName, OverallRating FROM RestaurantProfiles ORDER BY RestaurantName DESC";

                    // COmmand object
                    MySqlCommand cmd = new MySqlCommand(stm, instance.conn);
                    // Execute and run
                    MySqlDataReader rdr = cmd.ExecuteReader();

                    // Read info
                    while (rdr.Read())
                    {
                        string restName = rdr["RestaurantName"].ToString();
                        string rating = rdr["OverallRating"].ToString();
                        double stars;
                        if (double.TryParse(rating, out stars))
                        {

                            stars = Math.Round(stars, MidpointRounding.AwayFromZero);

                            if (stars == 0)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("*****");
                            }

                            if (stars < 1.50 && stars > 0)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("****");
                            }
                            if (stars > 1.49 && stars < 2.50)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("***");
                            }
                            if (stars > 2.49 && stars < 3.50)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("**");

                            }
                            if (stars > 3.49 && stars < 4.50)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("*");


                            }

                            if (stars > 4.49)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.WriteLine($"{starCount}");


                            }
                            Console.ForegroundColor = ConsoleColor.White;

                        }

                    }
                    Console.WriteLine("Press any key to return to the main menu.");
                    Console.ReadKey();
                    Console.Clear(); ;

                }
                else if (choice == 3)
                {
                    //SQL statement
                    string stm = "SELECT RestaurantName, OverallRating FROM RestaurantProfiles ORDER BY OverallRating DESC";

                    // COmmand object
                    MySqlCommand cmd = new MySqlCommand(stm, instance.conn);
                    // Execute and run
                    MySqlDataReader rdr = cmd.ExecuteReader();

                    // Read info
                    while (rdr.Read())
                    {
                        string restName = rdr["RestaurantName"].ToString();
                        string rating = rdr["OverallRating"].ToString();
                        double stars;
                        if (double.TryParse(rating, out stars))
                        {

                            stars = Math.Round(stars, MidpointRounding.AwayFromZero);


                            if (stars == 0)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("*****");
                            }

                            if (stars < 1.50 && stars > 0)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("****");
                            }
                            if (stars > 1.49 && stars < 2.50)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("***");
                            }
                            if (stars > 2.49 && stars < 3.50)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("**");

                            }
                            if (stars > 3.49 && stars < 4.50)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("*");


                            }

                            if (stars > 4.49)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.WriteLine($"{starCount}");


                            }
                            Console.ForegroundColor = ConsoleColor.White;

                        }

                    }
                    Console.WriteLine("Press any key to return to the main menu.");
                    Console.ReadKey();
                    Console.Clear(); ;
                }
                else if (choice == 4)
                {
                    //SQL statement
                    string stm = "SELECT RestaurantName, OverallRating FROM RestaurantProfiles ORDER BY OverallRating ASC";

                    // COmmand object
                    MySqlCommand cmd = new MySqlCommand(stm, instance.conn);
                    // Execute and run
                    MySqlDataReader rdr = cmd.ExecuteReader();

                    // Read info
                    while (rdr.Read())
                    {
                        string restName = rdr["RestaurantName"].ToString();
                        string rating = rdr["OverallRating"].ToString();
                        double stars;
                        if (double.TryParse(rating, out stars))
                        {

                            stars = Math.Round(stars, MidpointRounding.AwayFromZero);

                            if (stars == 0)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("*****");
                            }

                            if (stars < 1.50 && stars > 0)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("****");
                            }
                            if (stars > 1.49 && stars < 2.50)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("***");
                            }
                            if (stars > 2.49 && stars < 3.50)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("**");

                            }
                            if (stars > 3.49 && stars < 4.50)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.Write($"{starCount}");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("*");


                            }

                            if (stars > 4.49)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                string starCount = StarConvert.stars(stars);
                                Console.WriteLine($"{starCount}");


                            }
                            Console.ForegroundColor = ConsoleColor.White;

                        }

                    }
                    Console.WriteLine("Press any key to return to the main menu.");
                    Console.ReadKey();
                    Console.Clear(); ;
                }
                else if (choice == 5)
                {
                    Console.WriteLine("Would you like to...");
                    Console.Write(
                        "1.) See the best (5 Stars)\r\n" +
                        "2.) See 4 stars and better\r\n" +
                        "3.) See 3 stars and better\r\n" +
                        "4.) See the worst (1 star)\r\n" +
                        "5.) See unrated\r\n" +
                        "6.) Back\r\n" +
                        "");
                    string selection = Console.ReadLine();
                    int userSelection;
                    //Validate
                    while (!int.TryParse(selection, out userSelection))
                    {
                        Console.WriteLine("Invalid number. Only type the number associated with your choice.");
                        selection = Console.ReadLine();
                    }
                    if(userSelection == 1)
                    {
                        //SQL statement
                        string stm = "SELECT RestaurantName, OverallRating FROM RestaurantProfiles WHERE OverallRating > 4.49";
                        
                        // COmmand object
                        MySqlCommand cmd = new MySqlCommand(stm, instance.conn);
                        // Execute and run
                        MySqlDataReader rdr = cmd.ExecuteReader();

                        // Read info
                        while (rdr.Read())
                        {
                            string restName = rdr["RestaurantName"].ToString();
                            string rating = rdr["OverallRating"].ToString();
                            double stars;
                            if (double.TryParse(rating, out stars))
                            {

                                stars = Math.Round(stars, MidpointRounding.AwayFromZero);
                                
                                if (stars > 4.49)
                                {
                                    Console.ForegroundColor = ConsoleColor.White;
                                    Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                    string starCount = StarConvert.stars(stars);
                                    Console.WriteLine($"{starCount}");


                                }
                                Console.ForegroundColor = ConsoleColor.White;

                            }

                        }
                        Console.WriteLine("Press any key to return to the main menu.");
                        Console.ReadKey();
                        Console.Clear(); ;

                    }
                    else if (userSelection == 2)
                    {
                        //SQL statement
                        string stm = "SELECT RestaurantName, OverallRating FROM RestaurantProfiles WHERE OverallRating > 3.49 ORDER BY OverallRating ASC";

                        // COmmand object
                        MySqlCommand cmd = new MySqlCommand(stm, instance.conn);
                        // Execute and run
                        MySqlDataReader rdr = cmd.ExecuteReader();

                        // Read info
                        while (rdr.Read())
                        {
                            string restName = rdr["RestaurantName"].ToString();
                            string rating = rdr["OverallRating"].ToString();
                            double stars;
                            if (double.TryParse(rating, out stars))
                            {

                                stars = Math.Round(stars, MidpointRounding.AwayFromZero);
                                
                                if (stars > 3.49 && stars < 4.50)
                                {
                                    Console.ForegroundColor = ConsoleColor.White;
                                    Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                    string starCount = StarConvert.stars(stars);
                                    Console.Write($"{starCount}");
                                    Console.ForegroundColor = ConsoleColor.DarkGray;
                                    Console.WriteLine("*");

                                }

                                if (stars > 4.49)
                                {
                                    Console.ForegroundColor = ConsoleColor.White;
                                    Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                    string starCount = StarConvert.stars(stars);
                                    Console.WriteLine($"{starCount}");

                                }
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                        Console.WriteLine("Press any key to return to the main menu.");
                        Console.ReadKey();
                        Console.Clear(); ;
                    }
                    else if(userSelection == 3)
                    {
                        //SQL statement
                        string stm = "SELECT RestaurantName, OverallRating FROM RestaurantProfiles WHERE OverallRating > 2.49 ORDER BY OverallRating ASC";

                        // COmmand object
                        MySqlCommand cmd = new MySqlCommand(stm, instance.conn);
                        // Execute and run
                        MySqlDataReader rdr = cmd.ExecuteReader();

                        // Read info
                        while (rdr.Read())
                        {
                            string restName = rdr["RestaurantName"].ToString();
                            string rating = rdr["OverallRating"].ToString();
                            double stars;
                            if (double.TryParse(rating, out stars))
                            {
                                stars = Math.Round(stars, MidpointRounding.AwayFromZero);
                                if (stars > 2.49 && stars < 3.50)
                                {
                                    Console.ForegroundColor = ConsoleColor.White;
                                    Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                    string starCount = StarConvert.stars(stars);
                                    Console.Write($"{starCount}");
                                    Console.ForegroundColor = ConsoleColor.DarkGray;
                                    Console.WriteLine("**");

                                }
                                
                                if (stars > 3.49 && stars < 4.50)
                                {
                                    Console.ForegroundColor = ConsoleColor.White;
                                    Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                    string starCount = StarConvert.stars(stars);
                                    Console.Write($"{starCount}");
                                    Console.ForegroundColor = ConsoleColor.DarkGray;
                                    Console.WriteLine("*");

                                    
                                }
                                
                                if (stars > 4.49)
                                {
                                    Console.ForegroundColor = ConsoleColor.White;
                                    Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                    string starCount = StarConvert.stars(stars);
                                    Console.WriteLine($"{starCount}");
                                    

                                }
                                Console.ForegroundColor = ConsoleColor.White;

                            }

                        }
                        Console.WriteLine("Press any key to return to the main menu.");
                        Console.ReadKey();
                        Console.Clear();
                    }
                    else if(userSelection == 4)
                    {
                        //SQL statement
                        string stm = "SELECT RestaurantName, OverallRating FROM RestaurantProfiles WHERE OverallRating < 1.50 ORDER BY OverallRating DESC";

                        // COmmand object
                        MySqlCommand cmd = new MySqlCommand(stm, instance.conn);
                        // Execute and run
                        MySqlDataReader rdr = cmd.ExecuteReader();

                        // Read info
                        while (rdr.Read())
                        {
                            string restName = rdr["RestaurantName"].ToString();
                            string rating = rdr["OverallRating"].ToString();
                            double stars;
                            if (double.TryParse(rating, out stars))
                            {

                                stars = Math.Round(stars, MidpointRounding.AwayFromZero);

                                if( stars == 0)
                                {
                                    Console.ForegroundColor = ConsoleColor.White;
                                    Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                    string starCount = StarConvert.stars(stars);
                                    Console.Write($"{starCount}");
                                    Console.ForegroundColor = ConsoleColor.DarkGray;
                                    Console.WriteLine("*****");
                                }

                                if (stars < 1.50 && stars > 0)
                                {
                                    Console.ForegroundColor = ConsoleColor.White;
                                    Console.Write($"Restaurant name: {restName.PadRight(40)}      Rating: ");
                                    string starCount = StarConvert.stars(stars);
                                    Console.Write($"{starCount}");
                                    Console.ForegroundColor = ConsoleColor.DarkGray;
                                    Console.WriteLine("****");
                                }
                                
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                        Console.WriteLine("Press any key to return to the main menu.");
                        Console.ReadKey();
                        Console.Clear(); ;
                    }
                    else if(userSelection == 5)
                    {
                        //SQL statement
                        string stm = "SELECT RestaurantName, OverallRating FROM RestaurantProfiles WHERE OverallRating is NULL";

                        // COmmand object
                        MySqlCommand cmd = new MySqlCommand(stm, instance.conn);
                        // Execute and run
                        MySqlDataReader rdr = cmd.ExecuteReader();

                        // Read info
                        while (rdr.Read())
                        {
                            string restName = rdr["RestaurantName"].ToString();
                            string rating = rdr["OverallRating"].ToString();

                            Console.WriteLine($"Restaurant name: {restName.PadRight(40)}      Rating: Not Rated");

                        }
                        Console.WriteLine("Press any key to return to the main menu.");
                        Console.ReadKey();
                        Console.Clear();
                    }
                    else if (userSelection == 6)
                    {
                        Console.Clear();
                        Menu.DisplayMenu();
                    }
                }
                else if (choice == 6)
                {
                    Console.WriteLine("Press any key to return to the main menu.");
                    Console.ReadKey();

                }
            }
            catch (MySqlException e)
            {
                Console.WriteLine($"Error {e.Message}: ");
            }
            finally
            {
                if (instance.conn != null)
                {
                    instance.conn.Close();
                }
            }
        }
    }
}
