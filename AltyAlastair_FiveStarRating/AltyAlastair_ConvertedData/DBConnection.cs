﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace AltyAlastair_ConvertedData
{
    class DBConnection
    {
        public MySqlConnection conn = null;
        public static void Connect()
        {
            string cs = @"server=172.16.1.1;userid=dbsAdmin;password=password;database=SampleRestaurantDatabase;port=8889";
            DBConnection instance = new DBConnection();
            try
            {

                // try to open the database.
                instance.conn = new MySqlConnection(cs);
                instance.conn.Open();
            }
            catch (MySqlException e)
            {
                Console.WriteLine($"Error {e.Message}: ");
            }
            finally
            {
                if (instance.conn != null)
                {
                    instance.conn.Close();
                }
            }
        }

    }
}
