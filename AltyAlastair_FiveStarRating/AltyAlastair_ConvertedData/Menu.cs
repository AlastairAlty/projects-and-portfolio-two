﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AltyAlastair_ConvertedData
{
    class Menu
    {
        public static void DisplayMenu()
        {
            Console.Clear();
            // Create a menu
            Console.Write(
                "1.) Convert the restaurant profile table from SQL to JSON\r\n" +
                "2.) Showcase our 5 star rating system\r\n" +
                "3.) Showcase our animated bar graph review system\r\n" +
                "4.) Play a card game\r\n" +
                "0.) Exit\r\n" +
                "");
        }
    }
}
