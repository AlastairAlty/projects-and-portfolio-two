﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace AltyAlastair_ConvertedData
{
    class StarConvert
    {
        public static string stars(double y)
        {
            string asterisks = null;
            for (int i = 0; i < y; i++)
            {
                asterisks += "*";
                if(y > 3.49)
                {
                   Console.ForegroundColor = ConsoleColor.Green;
                }
                else if(y > 2.49 && y < 3.5)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                }
                else if(y < 2.5 && y > 0)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }
            }
            return asterisks;
        }
    }
}
