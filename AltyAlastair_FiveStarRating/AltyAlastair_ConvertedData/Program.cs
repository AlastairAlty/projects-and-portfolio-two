﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace AltyAlastair_ConvertedData
{
    class Program
    {


        static void Main(string[] args)
        {
            bool programIsRunning = true;
            while (programIsRunning)
            {
                Menu.DisplayMenu();

                // Catch the users menu choice and start a switch statement
                string menuChoice = Console.ReadLine().ToLower();
                switch (menuChoice)
                {
                    case "1":
                    case "convert the restaurant profile table from sql to json":
                        {
                            Console.WriteLine("Started file conversion.");
                            JSONConvert.ConvertData();
                        }
                        break;
                    case "2":
                    case "showcase our 5 star rating system":
                        {
                            ShowRating.Rating();
                        }
                        break;
                    case "3":
                    case "showcase out animated bar graph review system":
                        {

                        }
                        break;
                    case "4":
                    case "play a card game":
                        {

                        }
                        break;
                    case "0":
                    case "exit":
                        {
                            Console.WriteLine("Thank you for using my program, goodbye.");
                            Console.ReadKey();
                            programIsRunning = false;
                            
                        }
                        break;
                    default:
                        break;
                }
            }
            
        }
    }
}

       

