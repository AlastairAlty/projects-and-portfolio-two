﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace AltyAlastair_ConvertedData
{
    class JSONConvert
    {
        public MySqlConnection conn = null;
        //Output location
        static string outputFolder = @"..\..\..\Output";
        public static void ConvertData()
        {

            //Database Location
            //string cs = @"server= 127.0.0.1;userid=root;password=root;database=SampleRestaurantDatabase;port=8889";

            //Output Location
            //string _directory = @"../../output/";﻿﻿

            Directory.CreateDirectory(outputFolder);
            string cs = @"server=172.16.1.1;userid=dbsAdmin;password=password;database=SampleRestaurantDatabase;port=8889";

            JSONConvert instance = new JSONConvert();

            List<string> databaseResults = new List<string>();

            try
            {

                // try to open the database.
                instance.conn = new MySqlConnection(cs);
                instance.conn.Open();

                // Slect the info from the table
                DataTable data = instance.QueryDB("SELECT id, RestaurantName, Address, Phone, HoursOfOperation, Price, USACityLocation, Cuisine, FoodRating, ServiceRating, AmbienceRating, ValueRating, OverallRating, OverallPossibleRating FROM RestaurantProfiles");
                DataRowCollection rows = data.Rows;
                string startJsonArray = "[";
                string startValue = "{";
                string endValue = "}";
                string endJasonArray = "]";
                int counter = 0;
                foreach (DataRow row in rows)
                {
                    counter++;

                    startJsonArray += startValue + $"\"id\": \"{row["id"].ToString()}\"" + ", " +
                    $"\"Restaurant name\": \"{row["RestaurantName"].ToString()}\"" + ", " +
                    $"\"Address\": \"{row["Address"].ToString()}\"" + ", " +
                    $"\"Phone\": \"{row["Phone"].ToString()}\"" + ", " +
                    $"\"Hours\": \"{row["HoursOfOperation"].ToString()}\"" + ", " +
                    $"\"Price\": \"{row["Price"].ToString()}\"" + ", " +
                    $"\"Location\": \"{row["USACityLocation"].ToString()}\"" + ", " +
                    $"\"Cuisine\": \"{row["Cuisine"].ToString()}\"" + ", " +
                    $"\"Food rating\": \"{row["FoodRating"].ToString()}\"" + ", " +
                    $"\"Service rating\": \"{row["ServiceRating"].ToString()}\"" + ", " +
                    $"\"Abience rating\": \"{row["AmbienceRating"].ToString()}\"" + ", " +
                    $"\"Value rating\": \"{row["ValueRating"].ToString()}\"" + ", " +
                    $"\"Overall rating\": \"{row["OverallRating"].ToString()}\"" +
                    endValue + ", ";

                    if (counter == 100)
                    {
                        startJsonArray += startValue + $"\"id\": \"{row["id"].ToString()}\"" + ", " +
                    $"\"Restaurant name\": \"{row["RestaurantName"].ToString()}\"" + ", " +
                    $"\"Address\": \"{row["Address"].ToString()}\"" + ", " +
                    $"\"Phone\": \"{row["Phone"].ToString()}\"" + ", " +
                    $"\"Hours\": \"{row["HoursOfOperation"].ToString()}\"" + ", " +
                    $"\"Price\": \"{row["Price"].ToString()}\"" + ", " +
                    $"\"Location\": \"{row["USACityLocation"].ToString()}\"" + ", " +
                    $"\"Cuisine\": \"{row["Cuisine"].ToString()}\"" + ", " +
                    $"\"Food rating\": \"{row["FoodRating"].ToString()}\"" + ", " +
                    $"\"Service rating\": \"{row["ServiceRating"].ToString()}\"" + ", " +
                    $"\"Abience rating\": \"{row["AmbienceRating"].ToString()}\"" + ", " +
                    $"\"Value rating\": \"{row["ValueRating"].ToString()}\"" + ", " +
                    $"\"Overall rating\": \"{row["OverallRating"].ToString()}\"" +
                    endValue;
                    }

                }
                // Write JSON data to a file in Output
                File.WriteAllText(@"..\..\..\Output\AltyAlastair_ConvertedData" + ".json", startJsonArray + endJasonArray);
                Console.WriteLine("Finished converting file! File can be found in the Output folder.\n");
                Console.WriteLine("Press any key to return to the main menu.");
                Console.ReadKey();

            }
            catch (MySqlException e)
            {
                Console.WriteLine($"Error {e.Message}: ");
            }
            finally
            {
                if (instance.conn != null)
                {
                    instance.conn.Close();
                }
            }

        }
        DataTable QueryDB(string query)
        {
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, conn);
            DataTable data = new DataTable();

            adapter.SelectCommand.CommandType = CommandType.Text;
            adapter.Fill(data);

            return data;
        }
    }
}
