# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.26)
# Database: AlastairAlty_MDV229_Database_202005
# Generation Time: 2020-05-30 21:56:06 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table activityCategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `activityCategories`;

CREATE TABLE `activityCategories` (
  `activityCategoryId` int(11) NOT NULL,
  `categoryDescription` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`activityCategoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `activityCategories` WRITE;
/*!40000 ALTER TABLE `activityCategories` DISABLE KEYS */;

INSERT INTO `activityCategories` (`activityCategoryId`, `categoryDescription`)
VALUES
	(1,'Converting SQL to JSON'),
	(2,'Five Star Rating'),
	(3,'Animated Bar Graphs'),
	(4,'Card Game'),
	(5,'Repo'),
	(6,'Time Tracker EER & Database'),
	(7,'Time Tracker App'),
	(8,'Time Tracker Sprint 1'),
	(9,'Time Tracker Sprint 2'),
	(10,'Time Tracker Sprint 3'),
	(11,'Time Tracker Visual Story'),
	(12,'Researched App Flowchart'),
	(13,'Custom App Flowchart'),
	(14,'Custom App Code & Database'),
	(15,'Custom App Presentation'),
	(16,'Visual Story 1'),
	(17,'Visual Story 2'),
	(18,'Visual Story 3'),
	(19,'Review Test');

/*!40000 ALTER TABLE `activityCategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table activityDescriptions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `activityDescriptions`;

CREATE TABLE `activityDescriptions` (
  `activityDescriptionId` int(11) NOT NULL,
  `activityDescription` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`activityDescriptionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `activityDescriptions` WRITE;
/*!40000 ALTER TABLE `activityDescriptions` DISABLE KEYS */;

INSERT INTO `activityDescriptions` (`activityDescriptionId`, `activityDescription`)
VALUES
	(1,'Data Entry'),
	(2,'Coding'),
	(3,'Debugging'),
	(4,'Testing'),
	(5,'Uploading to FSO'),
	(6,'Reading Assignment'),
	(7,'Studying'),
	(8,'Thinking'),
	(9,'Staring at Computer Confused'),
	(10,'Writing'),
	(11,'Looking for Images'),
	(12,'Doing Assignment'),
	(13,'Logging Activity Times'),
	(14,'Recording Video');

/*!40000 ALTER TABLE `activityDescriptions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table activityLog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `activityLog`;

CREATE TABLE `activityLog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `calendarDay` int(11) DEFAULT NULL,
  `calendarDate` int(11) DEFAULT NULL,
  `dayName` int(11) DEFAULT NULL,
  `categoryDescription` int(11) DEFAULT NULL,
  `activityDescription` int(11) DEFAULT NULL,
  `timeSpentOnActivity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_idx` (`userId`),
  KEY `numericalDay_idx` (`calendarDay`),
  KEY `date_idx` (`calendarDate`),
  KEY `dayOfWeek_idx` (`dayName`),
  KEY `timeSpent_idx` (`timeSpentOnActivity`),
  KEY `activityDescription_idx` (`activityDescription`),
  KEY `activityCategory_idx` (`categoryDescription`),
  CONSTRAINT `CalendarDate` FOREIGN KEY (`calendarDate`) REFERENCES `trackedCalendarDates` (`calendarDateId`),
  CONSTRAINT `activityDescription` FOREIGN KEY (`activityDescription`) REFERENCES `activityDescriptions` (`activityDescriptionId`),
  CONSTRAINT `calendarDay` FOREIGN KEY (`calendarDay`) REFERENCES `trackedCalendarDays` (`calendarDayId`),
  CONSTRAINT `categoryDescription` FOREIGN KEY (`categoryDescription`) REFERENCES `activityCategories` (`activityCategoryId`),
  CONSTRAINT `dayName` FOREIGN KEY (`dayName`) REFERENCES `daysOfWeek` (`dayId`),
  CONSTRAINT `timeSpentOnActivity` FOREIGN KEY (`timeSpentOnActivity`) REFERENCES `activityTimes` (`activityTimeId`),
  CONSTRAINT `userId` FOREIGN KEY (`userId`) REFERENCES `timeTrackerUsers` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `activityLog` WRITE;
/*!40000 ALTER TABLE `activityLog` DISABLE KEYS */;

INSERT INTO `activityLog` (`id`, `userId`, `calendarDay`, `calendarDate`, `dayName`, `categoryDescription`, `activityDescription`, `timeSpentOnActivity`)
VALUES
	(1,1,2,2,2,1,7,2),
	(2,1,2,2,2,1,2,6),
	(3,1,2,2,2,1,3,2),
	(4,1,2,2,2,1,13,1),
	(5,1,2,2,2,13,8,2),
	(6,1,2,2,2,13,12,3),
	(7,1,3,3,3,13,8,2),
	(8,1,3,3,3,13,13,1),
	(9,1,3,3,3,16,7,4),
	(10,1,3,3,3,16,11,2),
	(11,1,3,3,3,16,10,2),
	(12,1,3,3,3,16,12,4),
	(13,1,4,4,4,16,12,1),
	(14,1,4,4,4,16,13,1),
	(15,1,4,4,4,6,6,2),
	(16,1,4,4,4,6,7,2),
	(17,1,4,4,4,6,12,2),
	(18,1,5,5,5,6,12,4),
	(19,1,5,5,5,6,7,2),
	(20,1,5,5,5,6,12,4),
	(21,1,5,5,5,6,13,2),
	(22,1,5,5,5,8,12,1),
	(23,1,8,8,1,2,7,4),
	(24,1,8,8,1,2,2,9),
	(25,1,8,8,1,2,4,2),
	(26,1,8,8,1,13,7,2),
	(27,1,9,9,2,13,12,3),
	(28,1,9,9,2,17,7,3),
	(29,1,9,9,2,17,10,6),
	(30,1,9,9,2,17,5,1),
	(31,1,9,9,2,7,7,2),
	(32,1,10,10,3,7,2,7),
	(33,1,10,10,3,7,3,3),
	(34,1,10,10,3,2,5,1),
	(35,1,11,11,4,13,5,1),
	(36,1,11,11,4,7,4,1),
	(37,1,12,12,5,7,7,4),
	(38,1,12,12,5,7,8,2),
	(39,1,12,12,5,7,2,9),
	(40,1,12,12,5,7,13,1),
	(41,1,12,12,5,7,3,3),
	(42,1,12,12,5,7,13,1),
	(43,1,13,13,6,9,13,1),
	(44,1,13,13,6,9,5,2),
	(45,1,15,15,1,3,6,1),
	(46,1,15,15,1,3,8,2),
	(47,1,15,15,1,3,2,4),
	(48,1,15,15,1,3,4,1),
	(49,1,15,15,1,3,9,1),
	(50,1,15,15,1,3,13,1),
	(51,1,16,16,2,18,6,1),
	(52,1,16,16,2,18,11,2),
	(53,1,16,16,2,18,7,4),
	(54,1,16,16,2,18,10,3),
	(55,1,17,17,3,14,6,1),
	(56,1,17,17,3,14,12,6),
	(57,1,17,17,3,14,2,4),
	(58,1,17,17,3,14,4,2),
	(59,1,17,17,3,18,5,1),
	(60,1,17,17,3,18,13,2),
	(61,1,17,17,3,14,13,2),
	(62,1,20,20,6,14,4,2),
	(63,1,20,20,6,14,3,2),
	(64,1,20,20,6,14,2,4),
	(65,1,21,21,7,14,14,2),
	(66,1,21,21,7,14,5,6),
	(67,1,21,21,7,14,13,2),
	(68,1,23,23,2,18,6,1),
	(69,1,23,23,2,18,10,4),
	(70,1,23,23,2,18,8,2),
	(71,1,23,23,2,18,11,2),
	(72,1,23,23,2,18,12,6),
	(73,1,24,24,3,19,7,3),
	(74,1,24,24,3,19,12,4),
	(75,1,24,24,3,19,13,1),
	(76,1,25,25,4,4,6,1),
	(77,1,25,25,4,4,6,1),
	(78,1,27,27,NULL,11,14,3),
	(79,1,27,27,NULL,11,2,1);

/*!40000 ALTER TABLE `activityLog` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table activityTimes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `activityTimes`;

CREATE TABLE `activityTimes` (
  `activityTimeId` int(11) NOT NULL,
  `timeSpentOnActivity` double NOT NULL,
  PRIMARY KEY (`activityTimeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `activityTimes` WRITE;
/*!40000 ALTER TABLE `activityTimes` DISABLE KEYS */;

INSERT INTO `activityTimes` (`activityTimeId`, `timeSpentOnActivity`)
VALUES
	(1,0.25),
	(2,0.5),
	(3,0.75),
	(4,1),
	(5,1.25),
	(6,1.5),
	(7,1.75),
	(8,2),
	(9,2.25),
	(10,2.5),
	(11,2.75),
	(12,3),
	(13,3.25),
	(14,3.5),
	(15,3.75),
	(16,4),
	(17,4.25),
	(18,4.5),
	(19,4.75),
	(20,5),
	(21,5.25),
	(22,5.5),
	(23,5.75),
	(24,6),
	(25,6.25),
	(26,6.5),
	(27,6.75),
	(28,7),
	(29,7.25),
	(30,7.5),
	(31,7.75),
	(32,8),
	(33,8.25),
	(34,8.5),
	(35,8.75),
	(36,9),
	(37,9.25),
	(38,9.5),
	(39,9.75),
	(40,10),
	(41,10.25),
	(42,10.5),
	(43,10.75),
	(44,11),
	(45,11.25),
	(46,11.5),
	(47,11.75),
	(48,12),
	(49,12.25),
	(50,12.5),
	(51,12.75),
	(52,13),
	(53,13.75),
	(54,14),
	(55,14.25),
	(56,14.5),
	(57,14.75),
	(58,15),
	(59,15.25),
	(60,15.5),
	(61,15.75),
	(62,16),
	(63,16.25),
	(64,16.5),
	(65,16.75),
	(66,17),
	(67,17.25),
	(68,17.5),
	(69,17.75),
	(70,18),
	(71,18.25),
	(72,18.5),
	(73,18.75),
	(74,19),
	(75,19.25),
	(76,19.5),
	(77,19.75),
	(78,20),
	(79,20.25),
	(80,20.5),
	(81,20.75),
	(82,21),
	(83,21.25),
	(84,21.5),
	(85,21.75),
	(86,22),
	(87,22.25),
	(88,22.5),
	(89,22.75),
	(90,23),
	(91,23.25),
	(92,23.5),
	(93,23.75),
	(94,24);

/*!40000 ALTER TABLE `activityTimes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table daysOfWeek
# ------------------------------------------------------------

DROP TABLE IF EXISTS `daysOfWeek`;

CREATE TABLE `daysOfWeek` (
  `dayId` int(11) NOT NULL,
  `dayName` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`dayId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `daysOfWeek` WRITE;
/*!40000 ALTER TABLE `daysOfWeek` DISABLE KEYS */;

INSERT INTO `daysOfWeek` (`dayId`, `dayName`)
VALUES
	(1,'Monday'),
	(2,'Tuesday'),
	(3,'Wednesday'),
	(4,'Thursday'),
	(5,'Friday'),
	(6,'Saturday'),
	(7,'Sunday');

/*!40000 ALTER TABLE `daysOfWeek` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table timeTrackerUsers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `timeTrackerUsers`;

CREATE TABLE `timeTrackerUsers` (
  `userId` int(11) NOT NULL,
  `userPassword` varchar(10) NOT NULL DEFAULT '',
  `userFirstname` varchar(25) NOT NULL DEFAULT '',
  `userLastname` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `timeTrackerUsers` WRITE;
/*!40000 ALTER TABLE `timeTrackerUsers` DISABLE KEYS */;

INSERT INTO `timeTrackerUsers` (`userId`, `userPassword`, `userFirstname`, `userLastname`)
VALUES
	(1,'password','Alastair','Alty'),
	(2,'password','admin','admin'),
	(3,'password','instructor','instructor');

/*!40000 ALTER TABLE `timeTrackerUsers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trackedCalendarDates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trackedCalendarDates`;

CREATE TABLE `trackedCalendarDates` (
  `calendarDateId` int(11) NOT NULL,
  `calendarDate` date NOT NULL,
  PRIMARY KEY (`calendarDateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `trackedCalendarDates` WRITE;
/*!40000 ALTER TABLE `trackedCalendarDates` DISABLE KEYS */;

INSERT INTO `trackedCalendarDates` (`calendarDateId`, `calendarDate`)
VALUES
	(1,'2020-05-04'),
	(2,'2020-05-05'),
	(3,'2020-05-06'),
	(4,'2020-05-07'),
	(5,'2020-05-08'),
	(6,'2020-05-09'),
	(7,'2020-05-10'),
	(8,'2020-05-11'),
	(9,'2020-05-12'),
	(10,'2020-05-13'),
	(11,'2020-05-14'),
	(12,'2020-05-15'),
	(13,'2020-05-16'),
	(14,'2020-05-17'),
	(15,'2020-05-18'),
	(16,'2020-05-19'),
	(17,'2020-05-20'),
	(18,'2020-05-21'),
	(19,'2020-05-22'),
	(20,'2020-05-23'),
	(21,'2020-05-24'),
	(22,'2020-05-25'),
	(23,'2020-05-26'),
	(24,'2020-05-27'),
	(25,'2020-05-28'),
	(26,'2020-05-29'),
	(27,'2020-05-30'),
	(28,'2020-05-31');

/*!40000 ALTER TABLE `trackedCalendarDates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trackedCalendarDays
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trackedCalendarDays`;

CREATE TABLE `trackedCalendarDays` (
  `calendarDayId` int(11) NOT NULL,
  `calendarNumericalDay` int(11) NOT NULL,
  PRIMARY KEY (`calendarDayId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `trackedCalendarDays` WRITE;
/*!40000 ALTER TABLE `trackedCalendarDays` DISABLE KEYS */;

INSERT INTO `trackedCalendarDays` (`calendarDayId`, `calendarNumericalDay`)
VALUES
	(1,1),
	(2,2),
	(3,3),
	(4,4),
	(5,5),
	(6,6),
	(7,7),
	(8,8),
	(9,9),
	(10,10),
	(11,11),
	(12,12),
	(13,13),
	(14,14),
	(15,15),
	(16,16),
	(17,17),
	(18,18),
	(19,19),
	(20,20),
	(21,21),
	(22,22),
	(23,23),
	(24,24),
	(25,25),
	(26,26),
	(27,27),
	(28,28);

/*!40000 ALTER TABLE `trackedCalendarDays` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
